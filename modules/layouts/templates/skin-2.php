<?php

/*

type: layout

name: Slider

position: 2

*/
?>

<div class="home-slider-section section edit safe-mode nodrop" field="layout-skin-2-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <module type="slickslider" />
        </div>
    </div>
</div>