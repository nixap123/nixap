<?php

/*

  type: layout

  name: Default

  description: Default template for bxSlider


*/

?>

<div class="slickslider home-slick-text-slider template-default col-sm-6 col-xs-12">
    <?php foreach ($data as $slide) { ?>
        <div class="home-slick-content">
            <h1><?php print $slide['primaryText']; ?></h1>
            <p><?php print $slide['secondaryText']; ?></p>
            <?php if (isset($slide['url']) AND $slide['url']): ?>
                <a href="<?php print $slide['url']; ?>" class="btn"><?php print $slide['seemoreText']; ?></a>
            <?php endif; ?>
        </div>
    <?php } ?>
</div>

<div class="slickslider-image home-slick-image-slider col-sm-6 col-xs-12">
    <?php foreach ($data as $slide) { ?>
        <div class="image" style="background-image: url('<?php print $slide['images'][0]; ?>');"></div>
    <?php } ?>
</div>