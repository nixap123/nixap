<?php


/*



type: layout



name: Dropdown cart



description: Dropdown cart template



*/


?>

<!-- Cart Products -->
<div class="all-cart-product clearfix mw-cart-<?php print $params['id'] ?> <?php print $template_css_prefix ?>">
    <?php if (is_array($data)) : ?>
        <?php foreach ($data as $item) : ?>
            <?php $pic = get_picture($item['rel_id']); ?>
            <?php $substrTitle = character_limiter($item['title'], 20, '...'); ?>

            <div class="single-cart clearfix mw-cart-item mw-cart-item-<?php print $item['id'] ?>">
                <div class="cart-image">
                    <a href="<?php print $item['url'] ?>"><img src="<?php print thumbnail($pic, 70, 70); ?>" alt=""/></a>
                </div>
                <div class="cart-info">
                    <h5><a href="product-details.html"><?php print $substrTitle; ?></a></h5>
                    <p><?php print $item['qty'] ?> x <?php print currency_format($item['price']); ?></p>
                    <a href="javascript:mw.cart.remove('<?php print $item['id'] ?>');" data-tip="<?php _e("Remove"); ?>" class="cart-delete tip" title="Remove this item"><i
                                class="pe-7s-trash"></i></a>
                </div>
            </div>
        <?php endforeach; ?>

        <?php
        if (!isset($params['checkout-link-enabled'])) {
            $checkout_link_enanbled = get_option('data-checkout-link-enabled', $params['id']);
        } else {
            $checkout_link_enanbled = $params['checkout-link-enabled'];
        }
        ?>

        <?php if ($checkout_link_enanbled != 'n') : ?>
            <?php $checkout_page = get_option('data-checkout-page', $params['id']); ?>

            <?php
            if ($checkout_page != false and strtolower($checkout_page) != 'default' and intval($checkout_page) > 0) {
                $checkout_page_link = content_link($checkout_page) . '/view:checkout';
            } else {
                $checkout_page_link = site_url('checkout');
            }
            ?>
        <?php endif; ?>
    <?php else : ?>
        <h3 style="padding: 5px 12px;font-weight:100;">Your cart is empty</h3>
    <?php endif; ?>

</div>

<div class="cart-totals">
    <h5>Total <span><?php print currency_format(cart_sum()); ?></span></h5>
</div>
<!-- Cart Button -->
<div class="cart-bottom  clearfix">
    <a href="<?php print $checkout_page_link; ?>"><?php _e("Go to checkout"); ?></a>
</div>
