<?php if (count($payment_options) > 0): ?>
    <h2 style="margin-top:0 " class="edit nodrop" field="checkout_payment_information_title" rel="global" rel_id="<?php print $params['id'] ?>">
        <?php _e("Payment method"); ?>
    </h2>
    <hr>
    <div class="payment-method">
        <div name="payment_gw" class="panel-group mw-payment-gateway mw-payment-gateway-<?php print $params['id']; ?>" id="accordion">
            <?php $count = 0;
            foreach ($payment_options as $payment_option) :
                $count++; ?>

                <div class="panel panel-default">
                    <div class="panel-heading" id="heading<?= $count ?>">
                        <h4 class="panel-title">
                            <input type="radio" <?php if ($count == 1): ?> checked="checked" <?php endif; ?> value="<?php print  $payment_option['gw_file']; ?>" name="payment_gw"
                                   id="inputID<?= $count ?>" />
                            <a class="select-payment" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $count ?>"><?php print  $payment_option['name']; ?></a>
                        </h4>
                    </div>
                    <div id="collapse<?= $count ?>" class="panel-collapse collapse <?php if ($count == 1): ?>in<?php endif; ?>">
                        <div class="panel-body">
                            <div id="mw-payment-gateway-selected-<?php print $params['id']; ?>">
                                <?php if (isset($payment_options[0])): ?>
                                    <module type="<?php print $payment_options[0]['gw_file'] ?>"/>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif; ?>

<script>
    $('.select-payment').on('click', function () {
        $(this).parent().find('input').prop('checked', true).trigger('change');
    });
</script>