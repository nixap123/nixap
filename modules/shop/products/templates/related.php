<?php

/*

type: layout

name: Related Products List

description: Related Products List layout

*/
?>

<script>
    $(document).ready(function () {
        $('.related-slider', '#<?php print $params['id']; ?>').slick({
            speed: 700,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="arrow-prev"><i class="fa fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="arrow-next"><i class="fa fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1169,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ]
        });
    });
</script>

<?php
$tn = $tn_size;
if (!isset($tn[0]) or ($tn[0]) == 150) {
    $tn[0] = 350;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}

?>

<?php if (!empty($data)): ?>
    <?php
    $count = 0;
    $len = count($data);

    $helpclass = '';

    if ($len % 3 != 0) {
        if ((($len - 1) % 3) == 0 or $len == 1) {
            $helpclass = 'last-row-single';
        } elseif ((($len - 2) % 3) == 0 or $len == 2) {
            $helpclass = 'last-row-twoitems';
        }
    }
    ?>

    <div class="<?php print $helpclass; ?> product-slider related-slider">
        <?php foreach ($data as $item): ?>
            <?php $count++; ?>

            <div class="col-xs-12" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                <div class="product-item text-center">
                    <!-- Product Image -->
                    <?php if ($show_fields == false or in_array('thumbnail', $show_fields)): ?>
                        <div class="product-img">
                            <!-- Image -->
                            <a href="<?php print $item['link'] ?>" class="image" style="background-image: url('<?php print thumbnail($item['image'], $tn[0], $tn[1]); ?>');">
                                <img src="<?php print thumbnail($item['image'], $tn[0], $tn[1]); ?>" alt=""/>
                            </a>

                            <!-- Action Button -->
                            <div class="action-btn-2">
                                <?php if ($show_fields == false or in_array('add_to_cart', $show_fields)): ?>
                                    <?php
                                    $add_cart_text = get_option('data-add-to-cart-text', $params['id']);
                                    if ($add_cart_text == false or $add_cart_text == "") {
                                        $add_cart_text = '';
                                    }
                                    ?>
                                    <?php if (is_array($item['prices'])): ?>
                                        <a href="javascript:;" onclick="mw.cart.add_item('<?php print $item['id'] ?>');" title="<?php print $add_cart_text ?>"><i class="pe-7s-cart"></i></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <a href="#" class="quick-view-modal" data-toggle="modal" data-target="#quickViewModal" title="Quick View" data-url="<?php print $item['link'] ?>"><i
                                            class="pe-7s-look"></i></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="product-info text-left">
                        <?php if ($show_fields == false or in_array('title', $show_fields)): ?>
                            <h5 class="title" itemprop="name"><a itemprop="url" href="<?php print $item['link'] ?>"><?php print character_limiter($item['title'], 35); ?></a></h5>
                        <?php endif; ?>
                        <?php if ($show_fields == false or in_array('price', $show_fields)): ?>
                            <div class="price-ratting fix">
                                <?php if (isset($item['prices']) and is_array($item['prices'])) : ?>
                                    <?php
                                    $vals2 = array_values($item['prices']);
                                    $val1 = array_shift($vals2); ?>
                                    <span class="price"><span class="new"><?php print currency_format($val1); ?></span></span>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?php if (is_array($item['prices'])): ?>
                            <?php foreach ($item['prices'] as $k => $v): ?>
                                <div class="mw-add-to-cart-<?php print $item['id'] . $count ?>">
                                    <input type="hidden" name="price" value="<?php print $v ?>"/>
                                    <input type="hidden" name="content_id" value="<?php print $item['id'] ?>"/>
                                </div>
                                <?php break; endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?>
<?php endif; ?>
