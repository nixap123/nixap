<?php

/*

type: layout

name: Default

description: Default cart template

*/

?>

<div>
    <?php if ($requires_registration and is_logged() == false): ?>
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="login-reg-form">
                <module type="users/register"/>
            </div>
        </div>
    <?php else: ?>

        <?php if ($payment_success == false): ?>


            <div class="clearfix"></div>
            <div class="col-xs-12">
                <div class="alert hide"></div>
            </div>
            <div class="clearfix"></div>

            <?php $cart_show_enanbled = get_option('data-show-cart', $params['id']); ?>
            <div <?php if ($step != 1): ?>style="display: none;"<?php endif; ?>>
                <?php if ($cart_show_enanbled != 'n'): ?>
                    <module type="shop/cart" template="big" id="cart_checkout_<?php print $params['id'] ?>" data-checkout-link-enabled="n"/>
                <?php endif; ?>

                <div class="cart-total col-xs-12">
                    <div class="proceed-to-checkout section mt-30">
                        <a href="?step=2">
                            <?php _e("Proceed to shipment"); ?>
                        </a>
                    </div>
                </div>
            </div>
            <div <?php if ($step != 2): ?>style="display: none;"<?php endif; ?>>
                <div class="">
                    <?php if ($user == false): ?>
                        <div class="col-md-12">
                            <div class="customer-login mb-40">
                                <!-- LOGIN START -->
                                <h3>Returning customer? <a data-toggle="collapse" href="#checkout-login">Click here to login</a></h3>
                                <div id="checkout-login" class="checkout-login collapse fix">
                                    <module type="users/login" template="default" class="checkout-login-wrapper mt-30"/>
                                </div>
                                <!-- LOGIN END -->
                            </div>
                        </div>
                    <?php endif; ?>
                    <form class="mw-checkout-form" id="checkout_form_<?php print $params['id'] ?>" method="post" action="<?php print api_link('checkout') ?>">
                        <div class="col-xs-12">
                            <div class="mw-checkout-responce mw-cart-data-holder"></div>
                        </div>
                        <div class="checkout-form">
                            <div class="col-lg-6 col-md-6 mb-20">
                                <h3 class="edit nodrop" field="checkout_personal_inforomation_title" rel="global"
                                    rel_id="<?php print $params['id'] ?>"><?php _e("Personal Information"); ?></h3>
                                <div class="row">

                                    <div class="col-sm-6 col-xs-12 mb-30">
                                        <label><?php _e("First Name"); ?></label>
                                        <input name="first_name" class="" type="text" value="<?php if (isset($user['first_name'])) {
                                            print $user['first_name'];
                                        } ?>"/>
                                    </div>

                                    <div class="col-sm-6 col-xs-12 mb-30">
                                        <label><?php _e("Last Name"); ?></label>
                                        <input name="last_name" class="" type="text" value="<?php if (isset($user['last_name'])) {
                                            print $user['last_name'];
                                        } ?>"/>
                                    </div>

                                    <div class="col-sm-6 col-xs-12 mb-30">
                                        <label><?php _e("Email"); ?></label>
                                        <input name="email" class="" type="text" value="<?php if (isset($user['email'])) {
                                            print $user['email'];
                                        } ?>"/>
                                    </div>


                                    <div class="col-sm-6 col-xs-12 mb-30">
                                        <label><?php _e("Phone"); ?></label>
                                        <input name="phone" class="form-control" type="text" value="<?php if (isset($user['phone'])) {
                                            print $user['phone'];
                                        } ?>"/>
                                    </div>

                                    <?php if ($cart_show_shipping != 'n'): ?>
                                        <div class="col-xs-12">
                                            <module type="shop/shipping" template="default"/>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <?php if ($user == false): ?>
                                    <div class="row">
                                        <div class="col-xs-12 mb-30">
                                            <input id="b_c_account" type="checkbox" data-target="createp_account"/>
                                            <label for="b_c_account">Create an account?</label>
                                            <div class="collapse mt-20" data-collapse="createp_account">
                                                <module type="users/register" template="default" class="checkout-login-wrapper mt-30"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-lg-6 col-md-6 col-xs-12 mb-40">
                                <div class="order-wrapper mb-30">
                                    <?php if ($cart_show_payments != 'n'): ?>
                                        <div class="mw-shipping-and-payment">
                                            <module type="shop/payments"/>
                                        </div>
                                    <?php endif; ?>
                                </div>


                                <div class="order-wrapper">
                                    <module type="shop/cart" template="checkout"/>

                                    <div class="payment-method">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <module type="shop/checkout/terms" />

                                                <div class="order-button mb-40">
                                                    <a href="?step=1" class="btn btn-default pull-left">
                                                        <?php _e("Go back to cart"); ?>
                                                    </a>

                                                    <button class="btn btn-primary pull-right" onclick="mw.cart.checkout('#checkout_form_<?php print $params['id'] ?>');" type="button"
                                                            id="complete_order_button" <?php if ($tems): ?> disabled="disabled"   <?php endif; ?>>
                                                        <?php _e("Complete order"); ?>
                                                    </button>
                                                    <div clas="clearfix"></div>
                                                </div>
                                                <div clas="clearfix"></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>


            </div>

            <div class="clear"></div>
            <div class="mw-checkout-responce"></div>
        <?php else: ?>
            <h2>
                <?php _e("Your payment was successfull."); ?>
            </h2>
        <?php endif; ?>
    <?php endif; ?>
</div>