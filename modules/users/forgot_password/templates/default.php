<script>mw.moduleCSS("<?php print modules_url(); ?>users/users_modules.css")</script>

<div class="module-forgot-password">
    <div id="form-holder{rand}">
        <h2>
            <?php if (!isset($form_title) or $form_title == false): ?>
                <?php _e("Forgot password"); ?>
            <?php else: ?>
                <?php print $form_title; ?>
            <?php endif; ?>
        </h2>
        <form id="user_forgot_password_form{rand}" method="post" class="clearfix checkout-form">
            <div class="row pb-20">
                <div class="col-xs-12 pb-20">
                    <label><?php _e("Email or Username"); ?></label>
                    <input type="text" name="username" placeholder="">
                </div>
            </div>

            <module type="captcha"/>
            <br>
            <button type="submit" class="btn btn-default pull-right"><?php print $form_btn_title; ?></button>
        </form>
        <div class="alert" style="margin: 0;display: none;"></div>
    </div>
</div>
