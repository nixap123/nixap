<?php

/*

type: layout
content_type: dynamic
name: Shop
is_shop: y
description: shop layout
position: 4
*/


?>
<?php include template_dir() . "header.php"; ?>


<div class="edit" rel="content" field="power_content">
    <module type="layouts" template="skin-1"/>

    <div class="page-section section pt-80 pb-120 nodrop">
        <div class="container">

            <div class="row">
                <module type="categories" content_id="<?php print PAGE_ID; ?>" template="horizontal-list-1"/>

                <div class="edit" field="content-shop" rel="page">
                    <module type="shop/products" limit="18" description-length="70"/>
                </div>
            </div>

        </div>
    </div>

    <!-- Do not delete this -->
</div>


<?php include template_dir() . "footer.php"; ?>
