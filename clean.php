<?php

/*

type: layout
content_type: static
name: Clean
position: 2
is_default: true
description: Clean layout

*/

?>
<?php include template_dir() . "header.php"; ?>

    <div class="edit" rel="content" field="power_content">
        <!-- Do not delete this comment! It is for PHP Parser -->
        <module type="layouts" template="skin-1"/>
        <module type="layouts" template="skin-6"/>
    </div>

<?php include template_dir() . "footer.php"; ?>